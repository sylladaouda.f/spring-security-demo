/**
 * 
 */
package ml.daouda.springsecuritydemo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ml.daouda.springsecuritydemo.model.Role;
import ml.daouda.springsecuritydemo.model.RoleName;

/**
 * @author Daouda Sylla
 * @Date 25 mars 2019
 * @Time 23:12:01
 *
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

	Optional<Role> findByName(RoleName roleName);
}
