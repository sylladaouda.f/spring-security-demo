/**
 * 
 */
package ml.daouda.springsecuritydemo.security;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

/**
 * @author Daouda Sylla
 * @Date 27 mars 2019
 * @Time 22:01:13
 *
 */
@Component
public class JwtTokenProvider {

	/**
	 * jwtSecret
	 */
	@Value("${app.jwtSecret}")
	private String jwtSecret;

	/**
	 * jwtExpirationInMs
	 */
	@Value("${app.jwtExpirationInMs}")
	private int jwtExpirationInMs;

	/**
	 * generateToken
	 * 
	 * @param authentication
	 * @return
	 */
	public String generateToken(Authentication authentication) {

		UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();

		Date now = new Date();
		Date expiryDate = new Date(now.getTime() + jwtExpirationInMs);

		return Jwts.builder().setSubject(Long.toString(userPrincipal.getId())).setIssuedAt(new Date())
				.setExpiration(expiryDate).signWith(SignatureAlgorithm.HS512, jwtSecret).compact();
	}

	/**
	 * getUserIdFromJWT
	 * 
	 * @param token
	 * @return
	 */
	public Long getUserIdFromJWT(String token) {
		Claims claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();

		return Long.parseLong(claims.getSubject());
	}

	/**
	 * validateToken
	 * 
	 * @param authToken
	 * @return
	 */
	public boolean validateToken(String authToken) {
		try {
			Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);

			return true;
		} catch (SignatureException e) {
			System.out.println("La signature JWT est invalide");
		} catch (MalformedJwtException e) {
			System.out.println("Le token JWT est invalide");
		} catch (ExpiredJwtException e) {
			System.out.println("Le token JWT est expiré");
		} catch (UnsupportedJwtException e) {
			System.out.println("Le token JWT n'est pas supporté");
		} catch (IllegalArgumentException e) {
			System.out.println("Le claims est vide");
		}

		return false;
	}
}
