/**
 * 
 */
package ml.daouda.springsecuritydemo.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ml.daouda.springsecuritydemo.model.User;
import ml.daouda.springsecuritydemo.repository.UserRepository;

/**
 * @author Daouda Sylla
 * @Date 27 mars 2019
 * @Time 21:48:17
 *
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	UserRepository userRepository;

	/* (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
	 */
	@Override
	@Transactional
	public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {

		User user = userRepository.findByUsernameOrEmail(usernameOrEmail, usernameOrEmail)
				.orElseThrow(() -> new UsernameNotFoundException("Utilisateur non trouvé avec : " + usernameOrEmail));

		return UserPrincipal.create(user);
	}

	// Methode utilisée par JwtAuthenticationFilter
	@Transactional
	public UserDetails loadUserById(Long id) {
		User user = userRepository.findById(id)
				.orElseThrow(() -> new UsernameNotFoundException("Utilisateur non trouvé avec : " + id));

		return UserPrincipal.create(user);
	}

}
