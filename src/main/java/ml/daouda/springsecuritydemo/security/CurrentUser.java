/**
 * 
 */
package ml.daouda.springsecuritydemo.security;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.security.core.annotation.AuthenticationPrincipal;

/**
 * @author Daouda Sylla
 * @Date 27 mars 2019
 * @Time 22:40:35
 *
 */
@Target({ ElementType.PARAMETER, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@AuthenticationPrincipal
public @interface CurrentUser {
	/*
	 * Annotation permettant de récuperer le user courant (redifinition
	 * de @AuthenticationPrincipal de spring)
	 */
}
