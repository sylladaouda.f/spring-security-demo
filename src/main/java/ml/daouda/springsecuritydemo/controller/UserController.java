/**
 * 
 */
package ml.daouda.springsecuritydemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ml.daouda.springsecuritydemo.exception.RessousceNotFoundException;
import ml.daouda.springsecuritydemo.model.User;
import ml.daouda.springsecuritydemo.payload.PagedResponse;
import ml.daouda.springsecuritydemo.payload.PollResponse;
import ml.daouda.springsecuritydemo.payload.UserIdentityAvailability;
import ml.daouda.springsecuritydemo.payload.UserProfile;
import ml.daouda.springsecuritydemo.payload.UserSummary;
import ml.daouda.springsecuritydemo.repository.PollRepository;
import ml.daouda.springsecuritydemo.repository.UserRepository;
import ml.daouda.springsecuritydemo.repository.VoteRepository;
import ml.daouda.springsecuritydemo.security.CurrentUser;
import ml.daouda.springsecuritydemo.security.UserPrincipal;
import ml.daouda.springsecuritydemo.service.PollService;
import ml.daouda.springsecuritydemo.util.AppConstants;

/**
 * @author Daouda Sylla
 * @Date 30 mars 2019
 * @Time 00:08:50
 *
 */
@RestController
@RequestMapping("/api")
public class UserController {

	/**
	 * userRepository
	 */
	@Autowired
	private UserRepository userRepository;

	/**
	 * pollRepository
	 */
	@Autowired
	private PollRepository pollRepository;

	/**
	 * voteRepository
	 */
	@Autowired
	private VoteRepository voteRepository;

	/**
	 * pollService
	 */
	@Autowired
	private PollService pollService;

	/**
	 * getCurrentUser
	 * 
	 * @param currentUser
	 * @return
	 */
	@GetMapping("/user/me")
	@PreAuthorize("hasRole('USER')")
	public UserSummary getCurrentUser(@CurrentUser UserPrincipal currentUser) {
		UserSummary userSummary = new UserSummary(currentUser.getId(), currentUser.getUsername(),
				currentUser.getName());
		return userSummary;
	}

	/**
	 * checkUsernameAvailability
	 * 
	 * @param username
	 * @return
	 */
	@GetMapping("/user/checkUsernameAvailability/{username}")
	public UserIdentityAvailability checkUsernameAvailability(@RequestParam(value = "username") String username) {
		Boolean isAvailable = !userRepository.existsByUsername(username);
		return new UserIdentityAvailability(isAvailable);
	}

	/**
	 * checkEmailAvailability
	 * 
	 * @param email
	 * @return
	 */
	@GetMapping("/user/checkEmailAvailability/{email}")
	public UserIdentityAvailability checkEmailAvailability(@RequestParam(value = "email") String email) {
		Boolean isAvailable = !userRepository.existsByEmail(email);
		return new UserIdentityAvailability(isAvailable);
	}

	/**
	 * getUserProfile
	 * 
	 * @param username
	 * @return
	 */
	@GetMapping("/users/{username}")
	public UserProfile getUserProfile(@PathVariable(value = "username") String username) {
		User user = userRepository.findByUsername(username)
				.orElseThrow(() -> new RessousceNotFoundException("User non trouvé " + username));

		long pollCount = pollRepository.countByCreatedBy(user.getId());
		long voteCount = voteRepository.countByUserId(user.getId());

		UserProfile userProfile = new UserProfile(user.getId(), user.getUsername(), user.getName(), user.getCreatedAt(),
				pollCount, voteCount);

		return userProfile;
	}

	/**
	 * getPollsCreatedBy
	 * 
	 * @param username
	 * @param currentUser
	 * @param page
	 * @param size
	 * @return
	 */
	@GetMapping("/users/{username}/polls")
	public PagedResponse<PollResponse> getPollsCreatedBy(@PathVariable(value = "username") String username,
			@CurrentUser UserPrincipal currentUser,
			@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
			@RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
		return pollService.getPollsCreatedBy(username, currentUser, page, size);
	}

	/**
	 * getPollsVotedBy
	 * 
	 * @param username
	 * @param currentUser
	 * @param page
	 * @param size
	 * @return
	 */
	@GetMapping("/users/{username}/votes")
	public PagedResponse<PollResponse> getPollsVotedBy(@PathVariable(value = "username") String username,
			@CurrentUser UserPrincipal currentUser,
			@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
			@RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
		return pollService.getPollsVotedBy(username, currentUser, page, size);
	}

}
