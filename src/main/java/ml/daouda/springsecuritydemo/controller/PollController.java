/**
 * 
 */
package ml.daouda.springsecuritydemo.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import ml.daouda.springsecuritydemo.model.Poll;
import ml.daouda.springsecuritydemo.payload.ApiResponse;
import ml.daouda.springsecuritydemo.payload.PagedResponse;
import ml.daouda.springsecuritydemo.payload.PollRequest;
import ml.daouda.springsecuritydemo.payload.PollResponse;
import ml.daouda.springsecuritydemo.payload.VoteRequest;
import ml.daouda.springsecuritydemo.security.CurrentUser;
import ml.daouda.springsecuritydemo.security.UserPrincipal;
import ml.daouda.springsecuritydemo.service.PollService;
import ml.daouda.springsecuritydemo.util.AppConstants;

/**
 * @author Daouda Sylla
 * @Date 29 mars 2019
 * @Time 23:39:49
 *
 */
@RestController
@RequestMapping("/api/polls")
public class PollController {

	/**
	 * pollService
	 */
	@Autowired
	private PollService pollService;

	/**
	 * getPolls
	 * 
	 * @param currentUser
	 * @param page
	 * @param size
	 * @return
	 */
	@GetMapping
	public PagedResponse<PollResponse> getPolls(@CurrentUser UserPrincipal currentUser,
			@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
			@RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
		return pollService.getAllPolls(currentUser, page, size);
	}

	/**
	 * createPoll
	 * 
	 * @param pollRequest
	 * @return
	 */
	@PostMapping
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<?> createPoll(@Valid @RequestBody PollRequest pollRequest) {
		Poll poll = pollService.createPoll(pollRequest);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{pollId}").buildAndExpand(poll.getId())
				.toUri();

		return ResponseEntity.created(location).body(new ApiResponse(true, "Poll Created Successfully"));
	}

	/**
	 * getPollById
	 * 
	 * @param currentUser
	 * @param pollId
	 * @return
	 */
	@GetMapping("/{pollId}")
	public PollResponse getPollById(@CurrentUser UserPrincipal currentUser, @PathVariable Long pollId) {
		return pollService.getPollById(pollId, currentUser);
	}

	/**
	 * castVote
	 * 
	 * @param currentUser
	 * @param pollId
	 * @param voteRequest
	 * @return
	 */
	@PostMapping("/{pollId}/votes")
	@PreAuthorize("hasRole('USER')")
	public PollResponse castVote(@CurrentUser UserPrincipal currentUser, @PathVariable Long pollId,
			@Valid @RequestBody VoteRequest voteRequest) {
		return pollService.castVoteAndGetUpdatedPoll(pollId, voteRequest, currentUser);
	}
}
