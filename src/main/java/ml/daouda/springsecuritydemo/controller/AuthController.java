/**
 * 
 */
package ml.daouda.springsecuritydemo.controller;

import java.net.URI;
import java.util.Collections;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import ml.daouda.springsecuritydemo.exception.AppException;
import ml.daouda.springsecuritydemo.model.Role;
import ml.daouda.springsecuritydemo.model.RoleName;
import ml.daouda.springsecuritydemo.model.User;
import ml.daouda.springsecuritydemo.payload.ApiResponse;
import ml.daouda.springsecuritydemo.payload.JwtAuthenticationResponse;
import ml.daouda.springsecuritydemo.payload.LoginRequest;
import ml.daouda.springsecuritydemo.payload.SignUpRequest;
import ml.daouda.springsecuritydemo.repository.RoleRepository;
import ml.daouda.springsecuritydemo.repository.UserRepository;
import ml.daouda.springsecuritydemo.security.JwtTokenProvider;

/**
 * @author Daouda Sylla
 * @Date 28 mars 2019
 * @Time 21:03:14
 *
 */
@RestController
@RequestMapping("/api/auth")
public class AuthController {

	/**
	 * authenticationManager : Objet de spring permettant d'authentifier un user
	 */
	@Autowired
	AuthenticationManager authenticationManager;

	/**
	 * passwordEncoder : Objet spring d'encodage de MDP
	 */
	@Autowired
	PasswordEncoder passwordEncoder;

	/**
	 * userRepository
	 */
	@Autowired
	UserRepository userRepository;

	/**
	 * roleRepository
	 */
	@Autowired
	RoleRepository roleRepository;

	/**
	 * jwtTokenProvider
	 */
	@Autowired
	JwtTokenProvider jwtTokenProvider;

	/**
	 * connecterUnUser
	 * 
	 * @param loginRequest
	 * @return
	 */
	@PostMapping("/signin")
	public ResponseEntity<?> connecterUnUser(@Valid @RequestBody final LoginRequest loginRequest) {

		/* On connecte l'user */
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsernameOrEmail(), loginRequest.getPassword()));

		/* On met le user connecté dans le context */
		SecurityContextHolder.getContext().setAuthentication(authentication);

		/* On génère le token */
		String token = jwtTokenProvider.generateToken(authentication);

		return ResponseEntity.ok(new JwtAuthenticationResponse(token));
	}

	/**
	 * enregistrerUnUser
	 * 
	 * @param signUpRequest
	 * @return
	 */
	@PostMapping("/signup")
	public ResponseEntity<?> enregistrerUnUser(@Valid @RequestBody final SignUpRequest signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return new ResponseEntity<>(new ApiResponse(false, "L'utilisateur existe déjà"),
					HttpStatus.BAD_REQUEST);
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return new ResponseEntity<>(new ApiResponse(false, "L'utilisateur existe déjà"),
					HttpStatus.BAD_REQUEST);
		}

		// Créons notre user
		User user = new User(signUpRequest.getNom(), signUpRequest.getUsername(), signUpRequest.getEmail(),
				signUpRequest.getPassword());

		// On encode le mdp
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		
		//On récupère les roles
		Role role = roleRepository.findByName(RoleName.ROLE_USER)
				.orElseThrow(() -> new AppException("Le role de l'utilisateur n'a pas été affecté"));
		
		user.setRoles(Collections.singleton(role));
		
		//Ajout du user dans la BDD
		User resultat = userRepository.save(user);
		
		//Génération de l'uri de création (Convention API rest)
		URI location = ServletUriComponentsBuilder
				.fromCurrentContextPath().path("/api/users/{username}")
				.buildAndExpand(resultat.getUsername()).toUri();
		
		return ResponseEntity.created(location).body(new ApiResponse(true, "L'utilisateur a bien été crée"));

	}
}
