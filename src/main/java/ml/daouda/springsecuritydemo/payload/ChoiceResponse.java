/**
 * 
 */
package ml.daouda.springsecuritydemo.payload;

/**
 * @author Daouda Sylla
 * @Date 29 mars 2019
 * @Time 23:21:58
 *
 */
public class ChoiceResponse {
	/**
	 * id
	 */
	private long id;
	/**
	 * text
	 */
	private String text;
	/**
	 * voteCount
	 */
	private long voteCount;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return the voteCount
	 */
	public long getVoteCount() {
		return voteCount;
	}

	/**
	 * @param voteCount the voteCount to set
	 */
	public void setVoteCount(long voteCount) {
		this.voteCount = voteCount;
	}

}
