/**
 * 
 */
package ml.daouda.springsecuritydemo.payload;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @author Daouda Sylla
 * @Date 28 mars 2019
 * @Time 20:37:42
 *
 */
public class SignUpRequest {

	/**
	 * nom
	 */
	@NotBlank
	@Size(min = 4, max = 40)
	private String nom;

	/**
	 * username
	 */
	@NotBlank
	@Size(min = 3, max = 15)
	private String username;

	/**
	 * email
	 */
	@NotBlank
	@Size(max = 40)
	@Email
	private String email;

	/**
	 * password
	 */
	@NotBlank
	@Size(min = 6, max = 20)
	private String password;

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}
