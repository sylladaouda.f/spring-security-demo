/**
 * 
 */
package ml.daouda.springsecuritydemo.payload;

import java.time.Instant;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Daouda Sylla
 * @Date 29 mars 2019
 * @Time 23:21:02
 *
 */
public class PollResponse {

	/**
	 * id
	 */
	private Long id;
	/**
	 * question
	 */
	private String question;
	/**
	 * choices
	 */
	private List<ChoiceResponse> choices;
	/**
	 * createdBy
	 */
	private UserSummary createdBy;
	/**
	 * creationDateTime
	 */
	private Instant creationDateTime;
	/**
	 * expirationDateTime
	 */
	private Instant expirationDateTime;
	/**
	 * isExpired
	 */
	private Boolean isExpired;

	/**
	 * selectedChoice
	 */
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long selectedChoice;
	/**
	 * totalVotes
	 */
	private Long totalVotes;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}

	/**
	 * @param question the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
	}

	/**
	 * @return the choices
	 */
	public List<ChoiceResponse> getChoices() {
		return choices;
	}

	/**
	 * @param choices the choices to set
	 */
	public void setChoices(List<ChoiceResponse> choices) {
		this.choices = choices;
	}

	/**
	 * @return the createdBy
	 */
	public UserSummary getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(UserSummary createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the creationDateTime
	 */
	public Instant getCreationDateTime() {
		return creationDateTime;
	}

	/**
	 * @param creationDateTime the creationDateTime to set
	 */
	public void setCreationDateTime(Instant creationDateTime) {
		this.creationDateTime = creationDateTime;
	}

	/**
	 * @return the expirationDateTime
	 */
	public Instant getExpirationDateTime() {
		return expirationDateTime;
	}

	/**
	 * @param expirationDateTime the expirationDateTime to set
	 */
	public void setExpirationDateTime(Instant expirationDateTime) {
		this.expirationDateTime = expirationDateTime;
	}

	/**
	 * @return the isExpired
	 */
	public Boolean getIsExpired() {
		return isExpired;
	}

	/**
	 * @param isExpired the isExpired to set
	 */
	public void setIsExpired(Boolean isExpired) {
		this.isExpired = isExpired;
	}

	/**
	 * @return the selectedChoice
	 */
	public Long getSelectedChoice() {
		return selectedChoice;
	}

	/**
	 * @param selectedChoice the selectedChoice to set
	 */
	public void setSelectedChoice(Long selectedChoice) {
		this.selectedChoice = selectedChoice;
	}

	/**
	 * @return the totalVotes
	 */
	public Long getTotalVotes() {
		return totalVotes;
	}

	/**
	 * @param totalVotes the totalVotes to set
	 */
	public void setTotalVotes(Long totalVotes) {
		this.totalVotes = totalVotes;
	}

}
