/**
 * 
 */
package ml.daouda.springsecuritydemo.payload;

/**
 * @author Daouda Sylla
 * @Date 28 mars 2019
 * @Time 20:46:44
 *
 */
public class ApiResponse {

	/**
	 * success
	 */
	private Boolean success;

	/**
	 * message
	 */
	private String message;

	/**
	 * @param success
	 * @param message
	 */
	public ApiResponse(Boolean success, String message) {
		this.success = success;
		this.message = message;
	}

	/**
	 * @return the success
	 */
	public Boolean getSuccess() {
		return success;
	}

	/**
	 * @param success the success to set
	 */
	public void setSuccess(Boolean success) {
		this.success = success;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
}
