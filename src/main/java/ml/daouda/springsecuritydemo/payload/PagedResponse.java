/**
 * 
 */
package ml.daouda.springsecuritydemo.payload;

import java.util.List;

/**
 * @author Daouda Sylla
 * @Date 29 mars 2019
 * @Time 23:26:43
 *
 */
public class PagedResponse<T> {

	/**
	 * content
	 */
	private List<T> content;
	/**
	 * page
	 */
	private int page;
	/**
	 * size
	 */
	private int size;
	/**
	 * totalElements
	 */
	private long totalElements;
	/**
	 * totalPages
	 */
	private int totalPages;
	/**
	 * last
	 */
	private boolean last;

	/**
	 * 
	 */
	public PagedResponse() {
	}

	/**
	 * @param content
	 * @param page
	 * @param size
	 * @param totalElements
	 * @param totalPages
	 * @param last
	 */
	public PagedResponse(List<T> content, int page, int size, long totalElements, int totalPages, boolean last) {
		this.content = content;
		this.page = page;
		this.size = size;
		this.totalElements = totalElements;
		this.totalPages = totalPages;
		this.last = last;
	}

	/**
	 * @return the content
	 */
	public List<T> getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(List<T> content) {
		this.content = content;
	}

	/**
	 * @return the page
	 */
	public int getPage() {
		return page;
	}

	/**
	 * @param page the page to set
	 */
	public void setPage(int page) {
		this.page = page;
	}

	/**
	 * @return the size
	 */
	public int getSize() {
		return size;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(int size) {
		this.size = size;
	}

	/**
	 * @return the totalElements
	 */
	public long getTotalElements() {
		return totalElements;
	}

	/**
	 * @param totalElements the totalElements to set
	 */
	public void setTotalElements(long totalElements) {
		this.totalElements = totalElements;
	}

	/**
	 * @return the totalPages
	 */
	public int getTotalPages() {
		return totalPages;
	}

	/**
	 * @param totalPages the totalPages to set
	 */
	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	/**
	 * @return the last
	 */
	public boolean isLast() {
		return last;
	}

	/**
	 * @param last the last to set
	 */
	public void setLast(boolean last) {
		this.last = last;
	}

}
