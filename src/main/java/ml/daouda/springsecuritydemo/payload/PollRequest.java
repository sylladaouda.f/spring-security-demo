/**
 * 
 */
package ml.daouda.springsecuritydemo.payload;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Daouda Sylla
 * @Date 29 mars 2019
 * @Time 23:09:40
 *
 */
public class PollRequest {

	/**
	 * question
	 */
	@NotBlank
	@Size(max = 140)
	private String question;

	/**
	 * choices
	 */
	@NotNull
	@Size(min = 2, max = 6)
	@Valid
	private List<ChoiceRequest> choices;

	/**
	 * pollLength
	 */
	@NotNull
	@Valid
	private PollLength pollLength;

	/**
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}

	/**
	 * @param question the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
	}

	/**
	 * @return the choices
	 */
	public List<ChoiceRequest> getChoices() {
		return choices;
	}

	/**
	 * @param choices the choices to set
	 */
	public void setChoices(List<ChoiceRequest> choices) {
		this.choices = choices;
	}

	/**
	 * @return the pollLength
	 */
	public PollLength getPollLength() {
		return pollLength;
	}

	/**
	 * @param pollLength the pollLength to set
	 */
	public void setPollLength(PollLength pollLength) {
		this.pollLength = pollLength;
	}

}
