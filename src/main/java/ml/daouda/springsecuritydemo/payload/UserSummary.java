/**
 * 
 */
package ml.daouda.springsecuritydemo.payload;

/**
 * @author Daouda Sylla
 * @Date 29 mars 2019
 * @Time 23:16:09
 *
 */
public class UserSummary {

	/**
	 * id
	 */
	private Long id;
	/**
	 * username
	 */
	private String username;
	/**
	 * name
	 */
	private String name;

	/**
	 * @param id
	 * @param username
	 * @param name
	 */
	public UserSummary(Long id, String username, String name) {
		this.id = id;
		this.username = username;
		this.name = name;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

}
