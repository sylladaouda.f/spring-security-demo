/**
 * 
 */
package ml.daouda.springsecuritydemo.payload;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @author Daouda Sylla
 * @Date 29 mars 2019
 * @Time 23:10:19
 *
 */
public class ChoiceRequest {

	/**
	 * text
	 */
	@NotBlank
	@Size(max = 40)
	private String text;

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

}
