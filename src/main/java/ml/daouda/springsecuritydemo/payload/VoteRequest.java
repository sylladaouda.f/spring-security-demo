/**
 * 
 */
package ml.daouda.springsecuritydemo.payload;

import javax.validation.constraints.NotNull;

/**
 * @author Daouda Sylla
 * @Date 29 mars 2019
 * @Time 23:14:08
 *
 */
public class VoteRequest {

	/**
	 * choiceId
	 */
	@NotNull
	private Long choiceId;

	/**
	 * @return the choiceId
	 */
	public Long getChoiceId() {
		return choiceId;
	}

	/**
	 * @param choiceId the choiceId to set
	 */
	public void setChoiceId(Long choiceId) {
		this.choiceId = choiceId;
	}

}
