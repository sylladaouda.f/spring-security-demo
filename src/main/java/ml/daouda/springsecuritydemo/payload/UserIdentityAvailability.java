/**
 * 
 */
package ml.daouda.springsecuritydemo.payload;

/**
 * @author Daouda Sylla
 * @Date 29 mars 2019
 * @Time 23:17:58
 *
 */
public class UserIdentityAvailability {

	/**
	 * available
	 */
	private Boolean available;

	/**
	 * @param available
	 */
	public UserIdentityAvailability(Boolean available) {
		this.available = available;
	}

	/**
	 * @return the available
	 */
	public Boolean getAvailable() {
		return available;
	}

	/**
	 * @param available the available to set
	 */
	public void setAvailable(Boolean available) {
		this.available = available;
	}

}
