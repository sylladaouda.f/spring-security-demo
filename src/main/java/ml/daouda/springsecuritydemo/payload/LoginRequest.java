/**
 * 
 */
package ml.daouda.springsecuritydemo.payload;

import javax.validation.constraints.NotBlank;

/**
 * @author Daouda Sylla
 * @Date 28 mars 2019
 * @Time 20:34:58
 *
 */
public class LoginRequest {

	/**
	 * usernameOrEmail
	 */
	@NotBlank
	private String usernameOrEmail;

	/**
	 * password
	 */
	@NotBlank
	private String password;

	/**
	 * @return the usernameOrEmail
	 */
	public String getUsernameOrEmail() {
		return usernameOrEmail;
	}

	/**
	 * @param usernameOrEmail the usernameOrEmail to set
	 */
	public void setUsernameOrEmail(String usernameOrEmail) {
		this.usernameOrEmail = usernameOrEmail;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}
