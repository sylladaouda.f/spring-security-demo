/**
 * 
 */
package ml.daouda.springsecuritydemo.payload;

/**
 * @author Daouda Sylla
 * @Date 28 mars 2019
 * @Time 20:43:23
 *
 */
public class JwtAuthenticationResponse {

	/**
	 * accessToken
	 */
	private String accessToken;

	/**
	 * tokenType
	 */
	private String tokenType = "Bearer";

	/**
	 * @param accessToken
	 */
	public JwtAuthenticationResponse(String accessToken) {
		this.accessToken = accessToken;
	}

	/**
	 * @return the accessToken
	 */
	public String getAccessToken() {
		return accessToken;
	}

	/**
	 * @param accessToken the accessToken to set
	 */
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	/**
	 * @return the tokenType
	 */
	public String getTokenType() {
		return tokenType;
	}

	/**
	 * @param tokenType the tokenType to set
	 */
	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

}
