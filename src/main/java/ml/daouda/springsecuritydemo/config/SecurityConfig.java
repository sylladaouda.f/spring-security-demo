/**
 * 
 */
package ml.daouda.springsecuritydemo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import ml.daouda.springsecuritydemo.security.CustomUserDetailsService;
import ml.daouda.springsecuritydemo.security.JwtAuthenticationEntryPoint;
import ml.daouda.springsecuritydemo.security.JwtAuthenticationFilter;

/**
 * @author Daouda Sylla
 * @Date 27 mars 2019
 * @Time 19:50:05
 *
 */
@Configuration
/*
 * Annotation utilisée par spring security afin d'activer la sécurité web sur
 * notre projet
 */
@EnableWebSecurity
/* Active la sécurité niveau méthode via l'annoatation @Secured */
@EnableGlobalMethodSecurity(securedEnabled = true /*
													 * Active l'utilisation de @Secured sur les méthodes de nos
													 * controllers/services
													 */, jsr250Enabled = true /*
																				 * Active la sécurisation
																				 * via @RolesAllowed
																				 */, prePostEnabled = true /*
																											 * Active l'
																											 * utilisation
																											 * de @PreAuthorize
																											 * /@
																											 * PostAuthorize
																											 * pour
																											 * permettre
																											 * des
																											 * expresions
																											 * complexe
																											 * de
																											 * contrôle
																											 * de
																											 * sécurité
																											 */)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	/*
	 * WebSecurityConfigurerAdapter : Elle implémente l'interface
	 * WebSecurityConfigurer de spring security. Elle fournie une configuration de
	 * sécurité par défaut et authorise d'autres classes à l'étendre afin de
	 * personnaliser certaine configurations en redéfinissant certaines méthodes
	 */

	/*
	 * Pour authentifier un utilisateur ou effectuer diverses vérifications basées
	 * sur les rôles, Spring a besoin de charger les détails de l'utilisateur pour
	 * cela, nous avons mis en place ce service pour récupérer le détail d'un user
	 * par son username en implémentant UserDetailsService de spring security
	 */
	@Autowired
	CustomUserDetailsService customUserDetailsService;

	/*
	 * Cette class implémente AuthenticationEntryPoint de spring security pour
	 * retourner l'erreur 401 unauthorized lorsqu'un client demande une ressource
	 * protégée par authentification
	 */
	@Autowired
	JwtAuthenticationEntryPoint unauthorizedHandler;

	/*
	 * Cette classe permettra de : 1. lire le token d'hautentification depuis le
	 * header Authorization de toutes les requêtes 2. de valider le token 3. de
	 * charger le détail de l'utilisateur associé à ce token 4. met le détail du
	 * user dans SecurityContext de spring security - Il faut savoir que spring
	 * security utilise le détail du user pour effectuer des vérifications
	 * d'authorisation. On peut aussi accéder au détail du userstocké dans
	 * SecurityContext depuis nos controllers pour effectuer notre logique business
	 */
	@Bean
	public JwtAuthenticationFilter jwtAuthenticationFilter() {
		return new JwtAuthenticationFilter();
	}

	/*
	 * AuthenticationManagerBuilder est utilisé pour créer une instance de
	 * AuthenticationManager qui est l'interface principale d'authentification
	 * utilisateur de spring security
	 */
	@Override
	public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
		authenticationManagerBuilder.userDetailsService(customUserDetailsService).passwordEncoder(passwordEncoder());
	}

	/*
	 * Nous allons utiliser le AuthenticationManager configuré pour authentifier un
	 * user via l'API de login
	 */
	@Bean(BeanIds.AUTHENTICATION_MANAGER)
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	/*
	 * La conf HttpSecurity est utilisé pour configurer les fonctionnalitées de
	 * sécurité comme csrf, sessionManagement et d'ajouter des règles de protection
	 * de ressources basées sur diverses conditions
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors()
				.and()
			.csrf()
				.disable()
			.exceptionHandling()
				.authenticationEntryPoint(unauthorizedHandler)
				.and()
			.sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				.and()
			.authorizeRequests()
				.antMatchers("/",
					"/favicon.ico",
					"/**/*.png",
					"/**/*.gif",
					"/**/*.svg",
					"/**/*.jpg",
					"/**/*.html",
					"/**/*.css",
					"/**/*.js")
					.permitAll()
				.antMatchers("/api/auth/**")
					.permitAll()
				.antMatchers("/api/user/checkUsernameAvailability", "/api/user/checkEmailAvailability")
					.permitAll()
				.antMatchers(HttpMethod.GET, "/api/polls/**", "/api/users/**")
					.permitAll()
				.anyRequest()
				.authenticated();
		
		//Ajout du filtrage de sécurité JWT personnalisé
		http.addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
	}
}
