/**
 * 
 */
package ml.daouda.springsecuritydemo.model;

/**
 * @author Daouda Sylla
 * @Date 29 mars 2019
 * @Time 22:57:53
 *
 */
public class ChoiceVoteCount {

	/**
	 * choiceId
	 */
	private Long choiceId;
	/**
	 * voteCount
	 */
	private Long voteCount;

	/**
	 * @param choiceId
	 * @param voteCount
	 */
	public ChoiceVoteCount(Long choiceId, Long voteCount) {
		this.choiceId = choiceId;
		this.voteCount = voteCount;
	}

	/**
	 * @return the choiceId
	 */
	public Long getChoiceId() {
		return choiceId;
	}

	/**
	 * @param choiceId the choiceId to set
	 */
	public void setChoiceId(Long choiceId) {
		this.choiceId = choiceId;
	}

	/**
	 * @return the voteCount
	 */
	public Long getVoteCount() {
		return voteCount;
	}

	/**
	 * @param voteCount the voteCount to set
	 */
	public void setVoteCount(Long voteCount) {
		this.voteCount = voteCount;
	}

}
