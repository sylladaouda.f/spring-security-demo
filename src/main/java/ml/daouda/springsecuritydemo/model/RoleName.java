/**
 * 
 */
package ml.daouda.springsecuritydemo.model;

/**
 * @author Daouda Sylla
 * @Date 25 mars 2019
 * @Time 22:50:19
 *
 */
public enum RoleName {

	ROLE_USER, ROLE_ADMIN
}
