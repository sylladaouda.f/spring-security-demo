/**
 * 
 */
package ml.daouda.springsecuritydemo.model;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import ml.daouda.springsecuritydemo.model.audit.UserDateAudit;

/**
 * @author Daouda Sylla
 * @Date 29 mars 2019
 * @Time 22:21:58
 *
 */
@Entity
@Table(name = "polls")
public class Poll extends UserDateAudit {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 2321225492232053986L;

	/**
	 * id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/**
	 * question
	 */
	@NotBlank
	@Size(max = 140)
	private String question;

	/**
	 * choices
	 */
	@OneToMany(mappedBy = "poll", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	@Size(min = 2, max = 6)
	@Fetch(FetchMode.SELECT)
	@BatchSize(size = 30)
	private List<Choice> choices = new ArrayList<>();

	/**
	 * expirationDateTime
	 */
	@NotNull
	private Instant expirationDateTime;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}

	/**
	 * @param question the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
	}

	/**
	 * @return the choices
	 */
	public List<Choice> getChoices() {
		return choices;
	}

	/**
	 * @param choices the choices to set
	 */
	public void setChoices(List<Choice> choices) {
		this.choices = choices;
	}

	/**
	 * @return the expirationDateTime
	 */
	public Instant getExpirationDateTime() {
		return expirationDateTime;
	}

	/**
	 * @param expirationDateTime the expirationDateTime to set
	 */
	public void setExpirationDateTime(Instant expirationDateTime) {
		this.expirationDateTime = expirationDateTime;
	}

	/**
	 * addChoice
	 * 
	 * @param choice
	 */
	public void addChoice(Choice choice) {
		choices.add(choice);
		choice.setPoll(this);
	}

	/**
	 * removeChoice
	 * 
	 * @param choice
	 */
	public void removeChoice(Choice choice) {
		choices.remove(choice);
		choice.setPoll(null);
	}
}
