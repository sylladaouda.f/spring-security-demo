/**
 * 
 */
package ml.daouda.springsecuritydemo.model.audit;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Daouda Sylla
 * @Date 29 mars 2019
 * @Time 21:46:53
 *
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdBy", "updatedBy" }, allowGetters = true)
public abstract class UserDateAudit extends DateAudit {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -6271494625460694744L;

	/**
	 * createdBy
	 */
	@Column(name = "created_by", nullable = false, updatable = false)
	@CreatedBy
	private Long createdBy;

	/**
	 * updatedBy
	 */
	@Column(name = "updated_by", nullable = false)
	@LastModifiedBy
	private Long updatedBy;

	/**
	 * @return the createdBy
	 */
	public Long getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the updatedBy
	 */
	public Long getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

}
