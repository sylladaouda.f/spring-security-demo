/**
 * 
 */
package ml.daouda.springsecuritydemo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Daouda Sylla
 * @Date 28 mars 2019
 * @Time 20:50:57
 *
 */
@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
public class AppException extends RuntimeException{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -647892634176229291L;

	/**
	 * @param message
	 * @param cause
	 */
	public AppException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public AppException(String message) {
		super(message);
	}

}
