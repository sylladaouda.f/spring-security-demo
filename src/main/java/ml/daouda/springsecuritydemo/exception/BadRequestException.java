/**
 * 
 */
package ml.daouda.springsecuritydemo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Daouda Sylla
 * @Date 28 mars 2019
 * @Time 20:55:42
 *
 */
@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -4463421717259772859L;

	/**
	 * resourceName
	 */
	private String resourceName;

	/**
	 * fieldName
	 */
	private String fieldName;

	/**
	 * fieldValue
	 */
	private Object fieldValue;

	/**
	 * @param resourceName
	 * @param fieldName
	 * @param fieldValue
	 */
	public BadRequestException(String resourceName, String fieldName, Object fieldValue) {
		super(String.format("%s n'a pas été trouvée avec %s : %s", resourceName, fieldName, fieldValue));
		this.resourceName = resourceName;
		this.fieldName = fieldName;
		this.fieldValue = fieldValue;
	}

	/**
	 * @param arg0
	 */
	public BadRequestException(String arg0) {
		super(arg0);
	}

	/**
	 * @return the resourceName
	 */
	public String getResourceName() {
		return resourceName;
	}

	/**
	 * @param resourceName the resourceName to set
	 */
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	/**
	 * @return the fieldName
	 */
	public String getFieldName() {
		return fieldName;
	}

	/**
	 * @param fieldName the fieldName to set
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 * @return the fieldValue
	 */
	public Object getFieldValue() {
		return fieldValue;
	}

	/**
	 * @param fieldValue the fieldValue to set
	 */
	public void setFieldValue(Object fieldValue) {
		this.fieldValue = fieldValue;
	}


}
