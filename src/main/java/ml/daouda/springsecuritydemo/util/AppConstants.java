/**
 * 
 */
package ml.daouda.springsecuritydemo.util;

/**
 * @author Daouda Sylla
 * @Date 29 mars 2019
 * @Time 23:30:34
 *
 */
public interface AppConstants {
	/**
	 * DEFAULT_PAGE_NUMBER
	 */
	String DEFAULT_PAGE_NUMBER = "0";
	/**
	 * DEFAULT_PAGE_SIZE
	 */
	String DEFAULT_PAGE_SIZE = "30";

	/**
	 * MAX_PAGE_SIZE
	 */
	int MAX_PAGE_SIZE = 50;
}
